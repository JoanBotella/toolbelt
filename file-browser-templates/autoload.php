<?php
declare(strict_types=1);

spl_autoload_register(function ($class) {
	$relPath = str_replace('\\', '/', $class).'.php';

	$path = __DIR__.'/'.$relPath;
	if (is_readable($path)) {
		require_once $path;
	}

}, true, false);