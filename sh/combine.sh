#! /bin/bash

function die {

	echo "$1" 1>&2
	exit $2

}

if [ $# -ne 2 ]
then
	die "Usage: ./combine.sh <source-directory-path> <output-file-path>" 1
fi

if [ ! -d $1 ]
then
	die "The first argument is not a directory." 2
fi

TMP_FILE="$2.tmp"

touch "$TMP_FILE"

if [ $? -gt 0 ]
then
	die "The temporary file could not be created." 3
fi

function combine {

	for fileName in `ls "$1"`
	do
		if [ -d "$1/$fileName" ]
		then
			combine "$1/$fileName" "$2"
		else

			cat "$1/$fileName" >> "$2"

			if [ $? -gt 0 ]
			then
				die "The contents of the file $1/$fileName could not be appended to $2" 4
			fi

		fi
	done

}

combine "$1" "$TMP_FILE"

mv "$TMP_FILE" "$2"

if [ $? -gt 0 ]
then
	die "The output file could not be written." 5
fi

