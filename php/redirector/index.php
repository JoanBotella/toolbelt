<?php

require_once('lib.php');

init();

if (!isset($_GET[SLUG_ARG]))
{
	http_response_code(HTTP_STATUS_BAD_REQUEST);
	die('You must add an slug to the URL.');
}

$slug = $_GET[SLUG_ARG];
$db = readFromDb();

if (!isset($db[$slug]))
{
	http_response_code(HTTP_STATUS_NOT_FOUND);
	die('The slug "'.$_GET[SLUG_ARG].'" does not exist.');
}

$header = 'Location: '.$db[$slug][URL_KEY];
header($header, true, 302);

$db[$slug][HITS_KEY]++;
$db[$slug][LAST_HIT_DATE_KEY] = date('Y-m-d H:i:s');

writeToDb($db);
