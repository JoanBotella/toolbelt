<?php

define('SLUG_ARG', 'slug');
define('DB_FILE', 'db.json');

define('SLUG_KEY', 'slug');
define('URL_KEY', 'url');
define('CREATION_DATE_KEY', 'creation_date');
define('HITS_KEY', 'hits');
define('LAST_HIT_DATE_KEY', 'last_hit_date');
