<?php

require_once('lib.php');

init();

if (isset($_POST[SLUG_KEY]))
{
	$oldDb = readFromDb();
	$db = [];

	$length = count($_POST[SLUG_KEY]);
	for($index = 0; $index < $length; $index++)
	{
		$slug = $_POST[SLUG_KEY][$index];
		$url = $_POST[URL_KEY][$index];

		if (
			$slug != ''
			&& $url != ''
		)
		{

			if (isset($oldDb[$slug]))
			{
				$creationDate = $oldDb[$slug][CREATION_DATE_KEY];
				$hits = (int) $oldDb[$slug][HITS_KEY];
				$lastHitDate = $oldDb[$slug][LAST_HIT_DATE_KEY];
			}
			else
			{
				$creationDate = date('Y-m-d H:i:s');
				$hits = 0;
				$lastHitDate = '';
			}

			$db[$slug] = [
				URL_KEY => $url,
				CREATION_DATE_KEY => $creationDate,
				HITS_KEY => $hits,
				LAST_HIT_DATE_KEY => $lastHitDate,
			];

		}
	}

	writeToDb($db);
}

?><!DOCTYPE html>
<html lang="en-EN" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN">
<head>
	<meta charset="UTF-8" />
	<title>Redirector</title>

	<style>

		body {
			margin: 0;
			padding: 1rem;
		}

		table {
			width: 100%;
			text-align: center;
			margin-top: 1rem;
		}

		input {
			text-align: center;
		}

	</style>

</head>
<body>

	<h1>Redirector</h1>

	<form action="" method="post">

		<div>
			<button>Save</button>
		</div>

		<table>
			<thead>
				<tr>
					<th>Slug</th>
					<th>URL</th>
					<th>Creation date</th>
					<th>Hits</th>
					<th>Last hit date</th>
				</tr>
			</thead>

			<tbody>
				<tr>
					<td>
						<input type="text" name="<?= SLUG_KEY ?>[]" value="" />
					</td>
					<td>
						<input type="url" name="<?= URL_KEY ?>[]" value="" />
					</td>
					<td></td>
					<td></td>
					<td></td>
				</tr>

<?php

	$db = readFromDb();
	foreach ($db as $slug => $row):

?>

				<tr>
					<td>
						<input type="text" name="<?= SLUG_KEY ?>[]" value="<?= $slug ?>" />
					</td>
					<td>
						<input type="url" name="<?= URL_KEY ?>[]" value="<?= $row[URL_KEY] ?>" />
					</td>
					<td><?= $row[CREATION_DATE_KEY] ?></td>
					<td><?= $row[HITS_KEY] ?></td>
					<td><?= $row[LAST_HIT_DATE_KEY] ?></td>
				</tr>

<?php

	endforeach

?>

			</tbody>
		</table>

	</form>

</body>
</html>