<?php

require_once('config.php');

define('HTTP_STATUS_BAD_REQUEST', 400);
define('HTTP_STATUS_NOT_FOUND', 404);

function writeToDb(array $db):void
{
	$result = @file_put_contents(DB_FILE, json_encode($db));
	if ($result === false)
	{
		throw new Exception('Db file could not be written');
	}
}

function readFromDb():array
{
	$result = @file_get_contents(DB_FILE);
	if ($result === false)
	{
		throw new Exception('Db file could not be read');
	}
	return json_decode($result, true);
}

function init():void
{
	if (!is_file(DB_FILE))
	{
		writeToDb([]);
	}
}
