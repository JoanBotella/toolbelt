<?php

if (isset($_POST['to']))
{

	$to = $_POST['to'];

	$subject =
		isset($_POST['subject'])
			? $_POST['subject']
			: ''
	;

	$message =
		isset($_POST['message'])
			? $_POST['message']
			: ''
	;

	$encoding = 'utf-8';

	$lineBreak = "\r\n";

	$subjectPreferences = [
		'input-charset' => $encoding,
		'output-charset' => $encoding,
		'line-length' => 76,
		'line-break-chars' => $lineBreak,
	];

	$headers = [
		'Content-type: text/html; charset='.$encoding,
	];

	$fromNameAndAddress = '';
	if (
		isset($_POST['fromAddress'])
		&& $_POST['fromAddress'] != ''
	)
	{
		$fromNameAndAddress = $_POST['fromAddress'];
	}
	if (
		isset($_POST['fromName'])
		&& $_POST['fromName'] != ''
	)
	{
		$fromNameAndAddress = $_POST['fromName'].' <'.$fromNameAndAddress.'>';
	}
	if ($fromNameAndAddress != '')
	{
		$headers[] = 'From: '.$fromNameAndAddress;
	}
	

	$headers[] = 'MIME-Version: 1.0';
	$headers[] = 'Content-Transfer-Encoding: 8bit';
	$headers[] = 'Date: '.date('r (T)');
	$headers[] = iconv_mime_encode('Subject', $subject, $subjectPreferences);

	$headersString = implode($lineBreak, $headers);

	// bool mail ( string $to , string $subject , string $message [, mixed $additional_headers [, string $additional_parameters ]] )

	$result = mail($to, $subject, $message, $headersString);

}

?><!DOCTYPE html>
<html lang="en-EN" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN">
<head>
	<meta charset="UTF-8" />
	<title>Mailer</title>

	<style>

		.success {
			color: green;
		}

		.failure {
			color: red;
		}

		label {
			display: block;
		}

		form div {
			margin-bottom: 1rem;
		}

	</style>

</head>
<body>

<?php

	if (isset($_POST['to']))
	{

		if ($result)
		{
			?>
			<p class="message success">The mail was successfully accepted for delivery.</p>
			<?php
		}
		else
		{
			?>
			<p class="message failure">The mail was NOT accepted for delivery.</p>
			<?php
		}

	}

?>

	<h1>Mailer</h1>

	<form action="" method="POST">

		<div>
			<label for="to">To</label>
			<input id="to" name="to" type="email" required="required" />
		</div>

		<div>
			<label for="fromName">From Name</label>
			<input id="fromName" name="fromName" type="text" />
		</div>

		<div>
			<label for="fromAddress">From Address</label>
			<input id="fromAddress" name="fromAddress" type="email" />
		</div>

		<div>
			<label for="subject">Subject</label>
			<input id="subject" name="subject" type="text" />
		</div>

		<div>
			<label for="message">Message</label>
			<textarea id="message" name="message"></textarea>
		</div>

		<input type="submit" value="Send" />

	</form>

</body>
</html>
